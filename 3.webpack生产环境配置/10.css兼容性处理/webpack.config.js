const HtmlWebpackPlugin = require('html-webpack-plugin');
const { resolve } = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');

// 设置nodejs环境变量
// process.env.NODE_ENV = 'development';

module.exports = {
    entry: './src/js/index.js',
    output:{
        filename: 'js/built.js',
        path: resolve(__dirname, 'build')
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use:[
                    MiniCssExtractPlugin.loader, 
                    // 将css文件整合到js文件中
                    'css-loader',
                    /* 
                        css兼容性处理：postcss --> postcss-loader postcss-preset-env
                       
                        帮postcss找到package.json中browserslist里面的配置，通过配置价值指定的css兼容性样式
                        // 新版本
                        "browserslist": [
                            "> 1%",
                            "last 2 versions"
                        ]
                        // 老版本
                        "browserslist": { 
                            // 开发环境 --> 设置node环境变量：process.env.NODE_ENV = development
                            "development":[
                            "last 1 chrome version",
                            "last 1 firefox version",
                            "last 1 safari version"
                            ],
                            // 生产环境：默认是看生产环境
                            "production":[
                            ">0.2%",
                            "not dead",
                            "not op_mini all"
                            ]
                        }
                    */
                    // 使用loader的默认配置
                    // 'postcss-loader'，
                    // 修改loader配置
                    {
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                  [
                                    "postcss-preset-env"
                                  ],
                                ],
                            },
                        }
                    }
                ]
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: './src/index.html'
        }),
        new MiniCssExtractPlugin({
            filename: 'css/index.css'
        }),
    ],
    mode: 'development'
}