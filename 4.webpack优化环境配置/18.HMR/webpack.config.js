/* 
   HMR: hot module replacement 热模块替换 / 模块热替换
        作用：一个模块发生变化，只会重新打包这一个模块（而不是打包所以模块）
            极大提升构建速度

            样式文件：可以使用HMR功能：因为style-loader内部实现了~
            js文件：target: "web", 5.0可以使用HMR功能。 4.0默认不能使用HMR功能 --> 需要修改js代码，添加支持HMR功能的代码
                4.0 HMR功能支持 需要在入口js文件加：
                    if(module.hot){
                        // 一旦 module.hot 为true,说明开启了HMR功能。-->让HMR功能代码生效
                        module.hot.accept('./pnint.js', function(){
                            // 方法会监听 print.js 文件的变化，一旦发生变化，其他模块不会重新打包构建。
                            // 会执行后面的回调函数
                            print();
                        })
                    }
            html文件：默认不能使用HMR功能，同时会导致问题：html文件不能热更新了~ （不需要HMR功能）
                解决: 修改entry入口，将html文件引入 
*/

const HtmlWebpackPlugin = require('html-webpack-plugin');
const { resolve }=require('path');

module.exports = {
    entry: ['./src/js/index.js','./src/index.html'],
    output: {
        filename:'js/built.js',
        path: resolve(__dirname, 'build')
    },
    module:{
        rules:[
            // loader的配置
            {
                // 处理less资源
                test: /\.less$/,
                use:['style-loader', 'css-loader', 'less-loader']
            },
            {
                // 处理css资源
                test: /\.css$/,
                use: ['style-loader','css-loader']
            },
            {
                // 处理图片资源
                test: /\.(jpg|png|gif)$/,
                loader: 'url-loader',
                options:{
                    limit: 8*1024,
                    name: '[hash:10].[ext]',
                    // 关闭es6模块化
                    esModule:false,
                    outputPath:'img'
                }
            },
            {
                // 处理html中的img资源
                test: /\.html$/,
                loader:'html-withimg-loader'
            },
            {
                // 处理其他资源
                exclude: /\.(html|js|css|less|jpg|png|gif)$/,
                loader:'file-loader',
                options:{
                    name: '[hash:10].[ext]',
                    outputPath:'media'
                }
            }
        ]
    },
    plugins: [
        // plugins的配置
        new HtmlWebpackPlugin({
            template:'./src/index.html'
        })
    ],
    mode: 'development',
     // webpack 5.0 自动刷新页面需要添加target: "web"属性
    target: "web",
    devServer:{
        contentBase: resolve(__dirname, 'build'),
        compress:true,
        port:3000,
        open:true,
        // 开启HMR功能
        // 当修改了webpack配置，新配置要想生效，必须重启webpack服务
        hot: true,
    }
}