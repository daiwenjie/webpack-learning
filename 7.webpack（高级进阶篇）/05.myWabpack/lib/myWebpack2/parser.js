const path = require('path');
const fs = require("fs");

const babelParser = require('@babel/parser');
const babelTraverse = require('@babel/traverse').default;
const { transformFromAst } = require('@babel/core');


const parser = {
    // 蒋文杰解析成ast
    getAst() {
        // 读取文件
        const file = fs.readFileSync(filePath, 'utf-8');
        
        // 2.将其解析成ast抽象语法树
        const ast = babelParser.parse(file, {
            sourceType: 'module' // 解析文件的模块化方案 ES Module
        })
        return ast;
    },
    // 获取依赖
    getDeps(ast, filePath) {
        const dirname = path.dirname(filePath);

        // 定义一个存储依赖的容器
        const deps = {};

        // 收集依赖
        babelTraverse(ast, {
            // 内部会遍历ast中program.body, 判断里面语句类型
            // 如果 type: ImportDeclaration 就会触发当前函数
            ImportDeclaration({node}) {
                // 文件相对路径：'./add.js'
               const relativePath = node.source.value;
               // 生成基于入口文件的绝对路径
               const absokutePath = path.resolve(dirname, relativePath);
               // 添加依赖
               deps[relativePath] = absokutePath;
            }
        })
        return deps;
    },
    // 将ast解析成code
    getCode(ast){
        const { code } = transformFromAst(ast, null, {
            presets: ['@babel/preset-env']
        })
        return code;
    }
}

module.exports = parser;