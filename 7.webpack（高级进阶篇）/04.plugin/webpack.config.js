// const Plugin = require('./plugins/Plugin');
// const Plugin2 = require('./plugins/Plugin2');
const CopyWenpackPlugin = require('./plugins/CopyWenpackPlugin');

module.exports = {
    plugins: [
        // new Plugin()
        // new Plugin2()
        new CopyWenpackPlugin({
            from: 'pubilc',
            to: 'css',
            ignore: ['**/index.html']
        })
    ]
}