const path = require('path');
const fs = require('fs');
const { promisify } = require('util');

const { validate } = require('schema-utils');
const globby = require('globby');
const webpack = require('webpack');

const schema = require('./schema.json');

const readFile = promisify(fs.readFile);
const { RawSource } = webpack.sources;

class CopyWebpackPlugin {
    constructor(options = {}){
        // 验证options是否符合规范
        validate(schema, options, {
            name: 'CopyWebpackPlugin'
        })

        this.options = options;
    }
    
    apply(compiler) {
        // 初始化compilation
        compiler.hooks.thisCompilation.tap('CopyWebpackPlugin', (compilation) => {
            // 添加资源的hooks
            compilation.hooks.additionalAssets.tapAsync('CopyWebpackPlugin', async (cb) => {
                // 将from中的资源复制到to中，输出出去
                const { from, ignore } = this.options;
                const to = this.options.to ? this.options.to : '.';
                
                // 1.过滤掉ignore的文件
                // globby(要处理的文件夹， options)
                const paths = await globby(from, { ignore });

                console.log(paths); // 所有要加载的文件路径数组

                // 2.读取from中所有资源

                const files = await Promise.all(
                    paths.map( async (absolutePath) => {
                        // 读取文件
                        const data = await readFile(absolutePath);
                        // basename得到最后的文件名称
                        const relativePath = path.basename(absolutePath);

                        // 和to属性结合
                        // 没有to --> index.css
                        // 有to --> css/index.css
                        const filename = path.join(to, relativePath);

                        return {
                            // 文件数据
                            data,
                            // 文件名称
                            filename
                        }
                    })
                )

                // 3.生成webpack格式的资源
                const assets = files.map((file) => {
                    const source = new RawSource(file.data);
                    return {
                        source,
                        filename: file.filename
                    }
                })
                // 4.添加compilation中，输出出去
                assets.forEach((assets) => {
                    compilation.emitAsset(assets.filename, assets.source);
                })

                cb();
            })
        })
    }
}

module.exports = CopyWebpackPlugin;