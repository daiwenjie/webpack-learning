const { SyncHook, SyncBailHook, AsyncParallelHook, AsyncSeriesHook} = require('tapable');

class lesson {
    constructor(){
        // 初始化hooks容器
        this.hooks = {
            // 同步hooks,任务依次执行
            // go: new SyncHook(['address'])
            // SyncBailHook: 一旦有返回值就会退出~
            go: new SyncBailHook(['address']),

            // 异步hooks
            // AsyncParallelHook: 异步并行
            // leave: new AsyncParallelHook(['name', 'age'])
            // AsyncSeriesHook: 异步串行
            leave: new AsyncSeriesHook(['name', 'age'])
        }
    }
    tap(){
        // 往hooks容器中注册事件/添加回调函数
        this.hooks.go.tap('class123', (address) => {
            console.log('class123', address);
            return 111;
        })
        this.hooks.go.tap('class124', (address) => {
            console.log('class124', address);
        })

        this.hooks.leave.tapAsync('class456', (name, age, cb) => {
            setTimeout(() => {
                console.log('class456', name, age);
                cb();
            }, 2000);
        })

        this.hooks.leave.tapPromise('class556', (name, age) => {
            return new Promise((res) => {
                setTimeout(() => {
                    console.log('class556', name, age);
                    res();
                }, 1000);
            })
        })
    }
    start() {
        // 触发hooks
        this.hooks.go.call('c123');
        this.hooks.leave.callAsync('jack',18, function(){
            // 代表所有leave容器的函数触发完了，才触发
            console.log('end~~~'); 
        });
    }
}

const l = new lesson();
l.tap();
l.start();